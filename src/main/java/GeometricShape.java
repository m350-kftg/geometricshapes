import java.awt.*;

public abstract class GeometricShape {
    protected Point referencePoint;

    public GeometricShape(Point referencePoint) {
        this.referencePoint = referencePoint;
    }

    // Verschiebt die geometrische Figur um die übergebenen Delta Werte in x- und y-Richtung
    public void move(Point delta){
        this.referencePoint.translate(delta.x, delta.y);
    }

    // Berechnet Fläche der geometrischen Figur
    public abstract double area();
}
