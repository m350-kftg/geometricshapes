import java.awt.*;

public class Square extends Rectangle {
    public Square(Point upperLeftCorner, double side) {
        super(upperLeftCorner, side, side);
    }
}