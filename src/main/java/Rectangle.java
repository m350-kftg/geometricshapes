import java.awt.*;

public class Rectangle extends GeometricShape{

    private double width;
    private double height;

    public Rectangle(Point upperLeftCorner, double width, double height) {
        super(upperLeftCorner);
        this.width = width;
        this.height = height;
    }

    @Override
    public double area() {
        return width * height;
    }

}
